package com.artemvolovyk.edu.hneu.lab6_7.service;

import com.artemvolovyk.edu.hneu.lab6_7.model.entity.Performance;

import java.util.List;

public interface PerformanceService {
    public void save(Performance performance);
    public Performance findById(Integer id);
    public void deleteById(Integer id);
    public List<Performance> findAll();
}
