package com.artemvolovyk.edu.hneu.lab6_7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentPerformanceSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentPerformanceSpringApplication.class, args);
    }

}
