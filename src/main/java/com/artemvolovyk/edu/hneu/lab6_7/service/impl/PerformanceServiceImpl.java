package com.artemvolovyk.edu.hneu.lab6_7.service.impl;

import com.artemvolovyk.edu.hneu.lab6_7.model.entity.Performance;
import com.artemvolovyk.edu.hneu.lab6_7.repository.PerformanceRepository;
import com.artemvolovyk.edu.hneu.lab6_7.service.PerformanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerformanceServiceImpl implements PerformanceService {

    private final PerformanceRepository performanceRepository;

    @Autowired
    PerformanceServiceImpl(PerformanceRepository performanceRepository) {
        this.performanceRepository = performanceRepository;
    }

    public void save(Performance performance) {
        performanceRepository.save(performance);
    }

    public Performance findById(Integer id) {
        return performanceRepository.findById(id).orElse(null);
    }

    public void deleteById(Integer id) {
        performanceRepository.deleteById(id);
    }

    public List<Performance> findAll() {
        return performanceRepository.findAll();
    }
}
