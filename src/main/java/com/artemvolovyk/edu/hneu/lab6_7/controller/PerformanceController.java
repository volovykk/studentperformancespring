package com.artemvolovyk.edu.hneu.lab6_7.controller;

import com.artemvolovyk.edu.hneu.lab6_7.model.PerformanceDto;
import com.artemvolovyk.edu.hneu.lab6_7.model.entity.Performance;
import com.artemvolovyk.edu.hneu.lab6_7.service.PerformanceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PerformanceController {


    private PerformanceService performanceService;
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    public PerformanceController(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }

    @GetMapping("")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/performances")
    public String showAllPerformances(Model model) {
        model.addAttribute("performances", performanceService.findAll());
        return "show_all";
    }

    @GetMapping("/performances/insert")
    public String showInsertForm(Model model) {
        model.addAttribute("performance", new PerformanceDto());
        return "insert_edit";
    }

    @PostMapping("/performances/insert")
    public String createPerformance(@ModelAttribute("performance") PerformanceDto performanceDto, Model model) {
        if (validateForm(performanceDto, model)) {
            return "insert_edit";
        }

        formatGradesForDB(performanceDto);
        Performance performance = mapper.map(performanceDto, Performance.class);
        performanceService.save(performance);
        return "redirect:/performances";
    }

    @GetMapping("/performances/edit/{id}")
    public String showEditForm(@PathVariable("id") String idString, Model model) {
        Integer id = Integer.parseInt(idString);
        Performance performance = performanceService.findById(id);
        model.addAttribute("performance", mapper.map(performance, PerformanceDto.class));
        return "insert_edit";
    }

    @PostMapping("/performances/edit/{id}")
    public String updatePerformance(@PathVariable("id") Integer id, @ModelAttribute("performance") PerformanceDto performanceDto, Model model) {
        if (validateForm(performanceDto, model)) {
            return "insert_edit";
        }

        formatGradesForDB(performanceDto);
        Performance performance = mapper.map(performanceDto, Performance.class);
        performance.setPerformanceId(id);
        performanceService.save(performance);
        return "redirect:/performances";
    }

    @GetMapping("/performances/delete/{id}")
    public String deletePerformance(@PathVariable("id") Integer id) {
        performanceService.deleteById(id);
        return "redirect:/performances";
    }

    public boolean validateForm(PerformanceDto performanceDto, Model model) {
        String subjectName = performanceDto.getSubjectName();
        String lastName = performanceDto.getLastName();
        String firstName = performanceDto.getFirstName();
        String middleName = performanceDto.getMiddleName();
        String groupNo = performanceDto.getGroupNo();
        Integer course = performanceDto.getCourse();
        Integer attendedLectures = performanceDto.getAttendedLectures();
        Integer labWorksCompleted = performanceDto.getLabWorksCompleted();
        String labWorksGrades = performanceDto.getLabWorksGrades();

        boolean foundError = false;

        if (
            (subjectName == null || subjectName.isEmpty())
                || (lastName == null || lastName.isEmpty())
                || (firstName == null || firstName.isEmpty())
                || (groupNo == null || groupNo.isEmpty())
                || (course == null)
                || (attendedLectures == null)
                || (labWorksCompleted == null)
        ) {
            model.addAttribute("emptyFieldsError", "Будь-ласка, заповніть усі обов'язкові поля");
            foundError = true;
        }

        if (subjectName != null && subjectName.length() > 100) {
            model.addAttribute("subjectNameError", "Назва предмету не має перевищувати 100 символів");
            foundError = true;
        }

        if (firstName != null
                && !firstName.isEmpty()
                && !firstName.matches("^([А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+){1,50}$")) {
            model.addAttribute("firstNameError", "Некоректний формат для імені");
            foundError = true;
        }

        if (lastName != null
                && !lastName.isEmpty()
                && !lastName.matches("(^([А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+)(-[А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+)*){1,50}$")) {
            model.addAttribute("lastNameError", "Некоректний формат для прізвища");
            foundError = true;
        }

        if (middleName != null
                && !middleName.isEmpty()
                && !middleName.matches("^([А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+){1,50}$"))
        {
            model.addAttribute("middleNameError", "Некоректний формат для по-батькові");
            foundError = true;
        }

        if (groupNo != null
                && !groupNo.isEmpty()
                && !groupNo.matches("^([0-9]+(\\.[0-9]+)*){1,100}$")) {
            model.addAttribute("groupNoError", "Некоректний формат для номеру групи");
            foundError = true;
        }

        if (labWorksGrades != null
                && !labWorksGrades.isEmpty()
                && !labWorksGrades.matches("^(ЛР[1-9]([0-9]+)?: [1-9]([0-9]+)?(, ЛР[1-9]([0-9]+)?: [1-9]([0-9]+)?)*){0,255}$")) {
            model.addAttribute("labWorksGradesError", "Некоректний формат оцінок за лаб. роботи: ЛР<номер>: <оцінка>, ...");
            foundError = true;
        }

        return foundError;
    }

    public void formatGradesForDB(PerformanceDto performanceDto) {
        String formatted_grades = performanceDto.getLabWorksGrades().replaceAll(", ", "<br>");
        performanceDto.setLabWorksGrades(formatted_grades);
    }
}
