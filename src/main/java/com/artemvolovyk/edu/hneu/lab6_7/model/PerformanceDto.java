package com.artemvolovyk.edu.hneu.lab6_7.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PerformanceDto {
    private Integer performanceId;
    private LocalDateTime date;
    private String subjectName;
    private String lastName;
    private String firstName;
    private String middleName;
    private String groupNo;
    private Integer course;
    private Integer attendedLectures;
    private Integer labWorksCompleted;
    private String labWorksGrades;
}
