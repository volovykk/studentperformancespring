package com.artemvolovyk.edu.hneu.lab6_7.repository;

import com.artemvolovyk.edu.hneu.lab6_7.model.entity.Performance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerformanceRepository extends JpaRepository<Performance, Integer> {
}
