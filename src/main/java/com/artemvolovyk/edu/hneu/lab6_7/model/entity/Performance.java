package com.artemvolovyk.edu.hneu.lab6_7.model.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Table (name = "performance")
@Data
public class Performance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "performance_id")
    private Integer performanceId;

    @Column(name = "date", nullable = false)
    private LocalDateTime date;

    @Column(name = "subject_name", nullable = false)
    private String subjectName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "middle_name", nullable = false)
    private String middleName;

    @Column(name = "group_no", nullable = false)
    private String groupNo;

    @Column(name = "course", nullable = false)
    private Integer course;

    @Column(name = "attended_lectures", columnDefinition = "INT DEFAULT '0'")
    private Integer attendedLectures;

    @Column(name = "lab_works_completed", columnDefinition = "INT DEFAULT '0'")
    private Integer labWorksCompleted;

    @Column(name = "lab_works_grades")
    private String labWorksGrades;

    @PrePersist
    protected void onCreate() {
        date =  LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        date = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return String.format("com.artemvolovyk.edu.hneu.kpp.lab4_5.model.Performance " +
                        "[performance_id=%s, " +
                        "date=%s, " +
                        "subject_name=%s, " +
                        "last_name=%s, " +
                        "first_name=%s, " +
                        "middle_name=%s, " +
                        "group_no=%s, " +
                        "course=%s, " +
                        "attended_lectures=%s, " +
                        "lab_works_completed=%s, " +
                        "lab_works_grades=%s]",
                performanceId, date, subjectName, lastName, firstName, middleName,
                groupNo, course, attendedLectures, labWorksCompleted, labWorksGrades);
    }
}
